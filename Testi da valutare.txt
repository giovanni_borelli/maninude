Un trio acustico che nasce da un inverno passato di fianco alle braci in una fredda taverna. La voce al femminile richiama le calde note del Soul, ritmo e melodia vengono dalle mani nude di uno spirito Rock'n Roll che he venduto l'anima la Blues.
Suono semplice e coinvolgente per una serata tutta da ascoltare.       


Alcuni classici della musica rock, nuove scoperte, ballad irrinunciabili e un po' di beat generation per un viaggio che dalle origini, passa per New Orleans e il profondo black fino a portarti lontano, sulla strada per il vecchio west.


Right to be wrong
Billie Jean
Come Toghether
Bitch
Old Man
Sitting on a dock of a bay
Nothing real but Love
All along the watchtower
Pugni Chiusi
Preghero'
Se perdo anche te
Bang Bang
Sono Bugiarda
Ragazzo di strada
Piece of my heart