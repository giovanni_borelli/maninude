//onLoad functions

var onloadfunctions = new Array();
var onloadpars = new Array();

function addFunctionOnLoad(functionName, functionPars)
{
	onloadfunctions.push(functionName);
	onloadpars.push(functionPars);
}

window.onload = function()
{
	for(i=0; i<onloadfunctions.length; i++)
	{
		onloadfunctions[i](onloadpars[i]);
	}
}

// END onLoad functions

//onResize functions

var onresizefunctions = new Array();
var onresizepars = new Array();

function addFunctionOnResize(functionName, functionPars)
{
	onresizefunctions.push(functionName);
	onresizepars.push(functionPars);
}

window.onresize = function()
{
	for(i=0; i<onresizefunctions.length; i++)
	{
		onresizefunctions[i](onresizepars[i]);
	}
}

//END onResize functions

//Funzione per ottenere le dimensioni della finestra del browser
//indipendentemente dal browser utilizzato
var WindowSize = Class.create({
    width: window.innerWidth || (window.document.documentElement.clientWidth || window.document.body.clientWidth),
    height: window.innerHeight || (window.document.documentElement.clientHeight || window.document.body.clientHeight)
});