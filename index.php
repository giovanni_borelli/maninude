<?php
 include 'common_header.php'; 
?>

<br>
<br>
<br>

<div id="home-div" class="content-block" style="text-align:center; height:1000px;">
	<img id='logo-img' src="img/logo.gif" />
	
	<h2>Acoustic Ensamble</h2>
</div>

<div class="content-spacer">

</div>



<div id="chi-div" class="content-block">

		<h2>Chi siamo</h2>
	<div class="chisiamo-container">
		<p>
			<i>
				"La musica vibra nell'aria e l'anima la raccoglie <br>
				le Mani Nude l'hanno generata <br>
				e ad un tratto<br>
				si canta."<br>
			</i>
		</p>
		<p>
			Un ensamble acustico che nasce da un inverno passato di fianco alle braci in una fredda taverna. Voce ricercata e di grande impatto, ritmo e melodia che provengono dalle mani nude di uno spirito blues che ha venduto l'anima al Rock'n Roll.<br/>
Suono semplice e coinvolgente per una serata tutta da ascoltare.  
		</p>
		<div class="portaits-container">
			
			<table>
				<tr>
					<td onmouseover="showHiddenText($('ale'));" onmouseout="hideShownText($('ale'));" style="width:100%; text-align:center">
						<h1>Alessandro, voce <a href="https://www.facebook.com/alessandro.esposito.501" target="_blank"><img class="social-network-small" src="img/facebook-small.png"/></a></h1>
						<img src="img/ale.jpg" style="width:230px;" id="ale"/>				
					</td>
					<td onmouseover="showHiddenText($('andre'));" onmouseout="hideShownText($('andre'));" style="width:100%; text-align:center">
						<h1>Andrea, chitarra <a href="http://www.facebook.com/andrea.angelini.982" target="_blank"><img class="social-network-small" src="img/facebook-small.png"/></a></h1>
						<img src="img/andre.jpg" style="width:230px;" id="andre"/>
					</td>			
				</tr>
				<tr>
					<td onmouseover="showHiddenText($('gio'));" onmouseout="hideShownText($('gio'));" style="width:100%; text-align:center">
						<h1>Giovanni, basso <a href="http://www.facebook.com/bigjoe84" target="_blank"><img class="social-network-small" src="img/facebook-small.png"/></a>
						<a href="https://plus.google.com/u/1/110059508624148163618" target="_blank"><img class="social-network-small" src="img/googleplus-small.png"/></a></h1>
						<img src="img/gio.jpg" style="width:230px;" id="gio"/>
					</td>
					<td onmouseover="showHiddenText($('pacchio'));" onmouseout="hideShownText($('pacchio'));" style="width:100%; text-align:center">
						<h1>Luca, cajon <a href="http://www.facebook.com/luca.pacchioni.7" target="_blank"><img class="social-network-small" src="img/facebook-small.png"/></a></h1>
						<img src="img/pacchio.jpg" style="width:230px;" id="pacchio"/>
					</td>
				</tr>
			</table>
		</div>
		
	</div>

</div>

<div class="content-spacer">
&nbsp
</div>

<div id="musica-div" class="content-block">
	<h2>Musica</h2>
	
	<div class="music-container">
		Alcuni classici della musica pop/rock, ballad irrinunciabili e un po' di musica italiana per una serata che dalle origini del blues porta fino ai giorni nostri.<br><br>
	<iframe width="100%" height="450" scrolling="no" src="https://www.youtube.com/embed/t3jIOgqYwXg?rel=0" frameborder="0" allowfullscreen></iframe>
		<br>
		<br>	
		
		
		<iframe width="100%" height="450" scrolling="no" frameborder="no" src="http://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Fplaylists%2F2627531&amp;auto_play=false&amp;show_artwork=true&amp;color=000000"></iframe>
	</div>
</div>

<div class="content-spacer">
</div>

<div id="news-div" class="content-block" style="height:1700px;">
	<h2>News e concerti</h2>
	
	<div class="news-container">
		<div class="left-column">
		
			<div class="event" style="border:3px solid black">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							07/02/2016
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/997635213628122/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							19.00
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://goo.gl/maps/ihXFr1CSH972" target="_blank">Stones cafe' - Vignola (MO)</a>
						</td>
						<td>
							<a target="_blank" href="https://goo.gl/maps/ihXFr1CSH972"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live allo storico Stones Cafe' di Vignola<br><br>
							I Mani Nude inaugurano il 2016 con una fantastica serata aperitivo allo storico Stones Cafe' di Vignola!
						</td>
					</tr>
				</table>
			</div>
		
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							13/03/2015
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/395515823954244/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							21.30
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://www.google.it/maps/place/Viale+Emilio+Po,+68,+41126+Modena+MO/@44.6543847,10.9029273,17z/data=!4m7!1m4!3m3!1s0x477fefa5ba8f179b:0x91640a93b12bfc4e!2sViale+Emilio+Po,+68,+41126+Modena+MO!3b1!3m1!1s0x477fefa5ba8f179b:0x91640a93b12bfc4e?hl=it" target="_blank">Sir Francis Drake Pub</a>
						</td>
						<td>
							<a target="_blank" href="https://www.google.it/maps/place/Viale+Emilio+Po,+68,+41126+Modena+MO/@44.6543847,10.9029273,17z/data=!4m7!1m4!3m3!1s0x477fefa5ba8f179b:0x91640a93b12bfc4e!2sViale+Emilio+Po,+68,+41126+Modena+MO!3b1!3m1!1s0x477fefa5ba8f179b:0x91640a93b12bfc4e?hl=it"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live al Sir Francis Drake Pub<br><br>
							I Mani Nude tornano al Sir Francis con tanta Tennent's Super e tanta bella musica!
						</td>
					</tr>
				</table>
			</div>
			
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							17/01/2015
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/872999956063585/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							22.30
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://www.google.it/maps/place/Viale+Emilio+Po,+68,+41126+Modena+MO/@44.6543847,10.9029273,17z/data=!4m7!1m4!3m3!1s0x477fefa5ba8f179b:0x91640a93b12bfc4e!2sViale+Emilio+Po,+68,+41126+Modena+MO!3b1!3m1!1s0x477fefa5ba8f179b:0x91640a93b12bfc4e?hl=it" target="_blank">Sir Francis Drake Pub</a>
						</td>
						<td>
							<a target="_blank" href="https://www.google.it/maps/place/Viale+Emilio+Po,+68,+41126+Modena+MO/@44.6543847,10.9029273,17z/data=!4m7!1m4!3m3!1s0x477fefa5ba8f179b:0x91640a93b12bfc4e!2sViale+Emilio+Po,+68,+41126+Modena+MO!3b1!3m1!1s0x477fefa5ba8f179b:0x91640a93b12bfc4e?hl=it"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live al Sir Francis Drake Pub<br><br>
							I Mani Nude inaugurano il 2015 con tanta nostalgia delle serate passate nel parcheggio asfaltato del Sir Francis con tanta Tennent's Super e tanta bella musica!
						</td>
					</tr>
				</table>
			</div>
			

			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							04/07/2014
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/1435961450008409/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							20.30
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://www.google.com/maps/place/Ristorante+Old+River/@44.476931,11.007351,15z/data=!4m2!3m1!1s0x0:0x296f75ef41a5da1a" target="_blank">Old River, Vignola</a>
						</td>
						<td>
							<a target="_blank" href="https://goo.gl/maps/oLTWy"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live @ Festa di Inizio Estate<br><br>
							Salutiamo l'inizio dell'estate 2014 al Old River di Vignola!<br>
							Evento con cena, live e dj-set di pre-apertura del Vignola Bistrot!
						</td>
					</tr>
				</table>
			</div>
			
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							16/05/2014
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/635102869915981/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							22.00
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://goo.gl/maps/oLTWy" target="_blank">Power Cafe, Ciano d'Enza</a>
						</td>
						<td>
							<a target="_blank" href="https://goo.gl/maps/oLTWy"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live @ Power Cafe'<br><br>
							Le Mani Nude tornano live al Power Cafe' di Ciano d'Enza per una fantastica seconda serata!<br>
						</td>
					</tr>
				</table>
			</div>
			
		
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							27/04/2014
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/1471925033039300/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							18.30
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://goo.gl/maps/oLTWy" target="_blank">Power Cafe, Ciano d'Enza</a>
						</td>
						<td>
							<a target="_blank" href="https://goo.gl/maps/oLTWy"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live @ Power Cafe'<br><br>
							Aperitivo musicale al Power Cafe'!<br>
						</td>
					</tr>
				</table>
			</div>
		
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Nuova Formazione!
						</td>
					</tr>
					
					<tr>
						<td class="event-content" colspan="3">
							Diamo un caloroso benvenuto ad Ale, il nuovo front-man dei Mani Nude!
						</td>
					</tr>
				</table>
			</div>
		
		<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							07/07/2012
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="http://www.facebook.com/events/390623454319795/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							21.30
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							Circolo Arci Migliarina, Carpi (MO)
						</td>
						<td>
							<a target="_blank" href="http://goo.gl/maps/5rpJ"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude al Circolo Arci di Migliarina<br><br>
							Birra a prezzi modici e buona musica!
						</td>
					</tr>
				</table>
			</div>		
		
			
<!-- 
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							20/05/2012
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/facebook.png" class="event-icon"/></a>
							<a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							21.00
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							Madison Square Garden (NY)
						</td>
						<td>
							<a target="_blank" href="http://g.co/maps/n42ju"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude esordiscono al Madison Square Garden!<br><br>
							Sold out!
						</td>
					</tr>
				</table>
			</div>
 -->
		
		</div>
		
		<div class="right-column">
		
			<div class="event" style="border-style:dashed">
					<table width="100%" cellspacing="5px">
						<tr>
							<td class="event-header" colspan="3">
								Mani Nude Live!
							</td>
						</tr>
						<tr>
							<td class="event-header">
								Data:
							</td>
							<td class="event-content">
								13/03/2015
							</td>
							<td rowspan="2" width="110px">
								<a target="_blank" href="https://www.facebook.com/events/646701042100607/"><img src="./img/facebook.png" class="event-icon"/></a>
								<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
							</td>
						</tr>
						<tr>
							<td class="event-header">
								Ore:
							</td>
							<td class="event-content">
								21.30
							</td>
						</tr>
						<tr>
							<td class="event-header">
								Luogo:
							</td>
							<td class="event-content">
								<a href="https://goo.gl/maps/muMdeBRScWo" target="_blank">Keller</a>
							</td>
							<td>
								<a target="_blank" href="https://goo.gl/maps/muMdeBRScWo"><img src="./img/gmaps.jpg" class="event-icon"/></a>
							</td>
						</tr>
						<tr>
							<td class="event-content" colspan="3">
								I Mani Nude Live al Keller Pub!<br><br>
								Come sempre buona birra e buona musica!
							</td>
						</tr>
					</table>
				</div>
		
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							20/02/2015
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/735913896506111/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							22.00
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://www.google.com/maps/place/Rock+Caf%C3%A8/@44.639875,10.930677,15z/data=!4m2!3m1!1s0x0:0x745e424986639a2f" target="_blank">Rock Cafe'</a>
						</td>
						<td>
							<a target="_blank" href="https://www.google.com/maps/place/Rock+Caf%C3%A8/@44.639875,10.930677,15z/data=!4m2!3m1!1s0x0:0x745e424986639a2f"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live allo storico Rock Cafe' a Modena!<br><br>
							Come sempre buona birra e buona musica!
						</td>
					</tr>
				</table>
			</div>
					
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							09/05/2014
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/238494423023662/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							20.30
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://goo.gl/maps/WEsG7" target="_blank">Costaiola on the Rock! Solignano</a>
						</td>
						<td>
							<a target="_blank" href="https://goo.gl/maps/WEsG7"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live al "Costaiola on the Rock!"<br><br>
							11^ edizione della festa che prevede due giorni dedicati al maiale nelle sue forme più prelibate e non solo!<br>
							A seguire concerto dei Big Ones!
						</td>
					</tr>
				</table>
			</div>
			
			
			
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							12/04/2014
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="https://www.facebook.com/events/1454547421446964//"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							21.00
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="https://goo.gl/maps/cvjbO" target="_blank">Polisportiva Gino Pini - Viale Pio la Torre, 61</a>
						</td>
						<td>
							<a target="_blank" href="https://goo.gl/maps/cvjbO"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live a "Lunga vita alle api.. e alle aziende agricole"<br><br>
							Cena sottoscrizione per dare un contributo alla ricostruzione di aziende agricole danneggiate nell'alluvione del 19/01/2014<br>
						</td>
					</tr>
				</table>
			</div>
						
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							15/12/2012
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="http://www.facebook.com/events/448261825236103/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							21.00
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="http://goo.gl/maps/urWkJ" target="_blank">Circolo Florida, Via Delfini 52 Modena</a>
						</td>
						<td>
							<a target="_blank" href="http://goo.gl/maps/urWkJ"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live a "Voci e musiche dei Diritti Umani"<br><br>
							Serata associazioni e realta' sociali del Modenese<br>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							03/08/2012
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="http://www.facebook.com/events/251250568326366/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							20.30
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							Campo Sportivo "F. Brioni", Gonzaga (MN)
						</td>
						<td>
							<a target="_blank" href="http://goo.gl/maps/Gwk0"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude Live a "Diamo un calcio al terremoto!"<br><br>
							Birra-Risto-Musica, ingresso offerta libera<br>
						</td>
					</tr>
				</table>
			</div>
		<!-- 

			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							25/05/2012
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/facebook.png" class="event-icon"/></a>
							<a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							21.00
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							Stadio San Siro (MI)
						</td>
						<td>
							<a target="_blank" href="http://g.co/maps/jwsw9"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude suonano live allo Stadio San Siro!<br><br>
							Chi non viene e' un bauscia!
						</td>
					</tr>
				</table>
			</div>
			
 -->
			
			
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Mani Nude Live!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							21/11/2012
						</td>
						<td rowspan="2" width="110px">
							<a target="_blank" href="http://www.facebook.com/events/458884514158041/"><img src="./img/facebook.png" class="event-icon"/></a>
							<!--a target="_blank" href="javascript:alert('Anche no :-)');"><img src="./img/gcal.png" class="event-icon"/></a-->
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Ore:
						</td>
						<td class="event-content">
							21.30
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Luogo:
						</td>
						<td class="event-content">
							<a href="http://goo.gl/maps/umPNE" target="_blank">Lab16, via Zamboni 16/D Bologna</a>
						</td>
						<td>
							<a target="_blank" href="http://goo.gl/maps/umPNE"><img src="./img/gmaps.jpg" class="event-icon"/></a>
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="3">
							I Mani Nude partecipano al live contest
							di Radio Fujiko!<br><br>
							<i>Esito: secondi della serata a un soffio dai primi!</i>
						</td>
					</tr>
				</table>
			</div>
			
			
			<div class="event" style="border-style:dashed">
				<table width="100%" cellspacing="5px">
					<tr>
						<td class="event-header" colspan="3">
							Il nuovo sito e' online!
						</td>
					</tr>
					<tr>
						<td class="event-header">
							Data:
						</td>
						<td class="event-content">
							19/05/2012
						</td>
					</tr>
					<tr>
						<td class="event-content" colspan="2">
							Il nuovo sito dei Mani Nude e' finalmente online!<br>
							Ora la domanda che rimane e': ma ne esisteva per caso uno vecchio?<br>
							E la risposta e': NO!<br>
							:-)<br><br>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<a href="http://www.manude.com">http://www.manude.com</a>
						</td>
					</tr>
				</table>
			</div>	
			
		
			
		</div>		
		
		

	</div>
	
	
</div>

<div class="content-spacer">
</div>
<div class="content-spacer">
</div>

<div id="contatti-div" class="content-block">
	<h2>Contatti</h2>
		
	<div class="contatti-container">
		
		<div class="email-container">
			<form method="POST" action="./email.php" target="_blank">
				<table style="border: 1px dashed black; padding: 20px;" width="100%">
					<tr>
						<td>
							Nome: <input class="email-text" type="text" name="nome"/>
						</td>
						<td>
							Email: <input class="email-text" type="text" name="email"/>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							Corpo del messaggio: <textarea class="email-text" name="testo" rows="10" cols="35"></textarea>
							<input class="email-submit" type="submit" value="" name="invia"/>
						</td>
					</tr>
				</table>			
			</form>			
		</div>
		
		<div class="social-container">
			<a href="https://www.facebook.com/pages/Mani-Nude/400549919984073" target="_blank"><img src="./img/facebook.png"/></a>
			<a href="http://soundcloud.com/maninude" target="_blank"><img src="./img/soundcloud.png"/></a>
			<a href="https://twitter.com/#!/Mani_Nude" target="_blank"><img src="./img/twitter.png"/></a>
			<a href="javascript:alert('Coming soon!');"><img src="./img/googleplus.png"/></a>		
		</div>
	</div>		
	
</div>


<?php
 include 'common_footer.php';
?>