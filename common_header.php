<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>

 <title>Mani Nude Acoustic Ensamble</title>
 <script src="js/prototype.js" type="text/javascript"></script>
 <script src="js/scriptaculous.js" type="text/javascript"></script>
 <script src="js/maninude.js" type="text/javascript"></script> 
 
 <link rel="stylesheet" type="text/css" href="css/stylesheet.css" ></link>
 <link rel="image_src" href="http://www.manude.com/img/logo.gif" />
  
 
 <meta name="description" content="Mani Nude acoustic ensamble: La musica vibra nell'aria e l'anima la raccoglie le Mani Nude l'hanno generata e ad un tratto si canta." />
 <meta name="keywords" content="Mani,Nude,Mani Nude,Musica,Live,Modena,Acoustic,Acustica,Ensamble,ManiNude,Manude" />
 <meta name="author" content="Giovanni Borelli" />
 <meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<script type="text/javascript">

	addFunctionOnLoad(setContentSize, {});
	addFunctionOnResize(setContentSize, {});
	addFunctionOnLoad(moveElements, {});
	
	var containerBlocks = null;
	var containerBlocksMinWidth = null;
	var mainImage = null;
	var mainImageRatio = null;
	var mainImageRatioReverse = null;
	
	function setContentSize()
	{
		//Cache degli elementi contenitori
		//per evitare la ricerca nel DOM ad ogni resize
		if(containerBlocks == null)
		{
			contentBlocks = $$('.content-block');
			contentBlocks.push($('header-block'));
			containerBlocksMinWidth = $$('.chisiamo-container')[0].getWidth();
			mainImage = $('logo-img');
			mainImageRatio = mainImage.getWidth() / mainImage.getHeight();
			mainImageRatioReverse = 1 / mainImageRatio;
		}
		
		/**
		windowWidth = new WindowSize().width;	
		windowHeight = new WindowSize().height;
		*/

		var viewport = document.viewport.getDimensions(); // Gets the viewport as an object literal
		var windowWidth = viewport.width; // Usable window width
		var windowHeight = viewport.height; // Usable window height
		
		blockWidth = windowWidth / 1.5;
		
		if(blockWidth < containerBlocksMinWidth)
		{
			blockWidth = containerBlocksMinWidth;
		}
		
		for(var i=0; i<contentBlocks.length; i++)
		{
			contentBlocks[i].morph('width:'+blockWidth+'px;');
		}
		
		finalImageHeight = windowHeight - 150;
		finalImageWidth = finalImageHeight * mainImageRatio;
		
		if(finalImageWidth > blockWidth)
		{
			finalImageWidth = blockWidth - 50;
			finalImageHeight = finalImageWidth * mainImageRatioReverse;
			mainImage.morph('width:'+finalImageWidth.toFixed(1)+'px; height:'+finalImageHeight.toFixed(1)+'px;');
		}else
		{
			mainImage.morph('height:'+finalImageHeight.toFixed(1)+'px; width:'+finalImageWidth.toFixed(1)+'px;');
		}
		
		return false;
	}
	
	function overMenu(element)
	{
		element.setStyle({backgroundColor: 'White', color: 'Black'});
		imgElement = element.down('img');
		
		if(imgElement != null)
		{
			source = imgElement.src.substring(0,imgElement.src.lastIndexOf('-')+1);
			source = source+"black.png";
			
			imgElement.src = source;
		}
	}
	
	function outMenu(element)
	{
		element.setStyle({backgroundColor: 'Black', color: 'White'});
		imgElement = element.down('img');
		
		if(imgElement != null)
		{
			source = imgElement.src.substring(0,imgElement.src.lastIndexOf('-')+1);
			source = source+"white.png";
			
			imgElement.src = source;
		}
	}
	
	function goTo(dest)
	{
		Effect.ScrollTo(dest, { offset:-50 }); 
		return false;
	}
	

	var elementsMovement = {"ale" : {'x':0, 'y':-65}, "gio" : {'x':0, 'y':-65}, "andre" : {'x':0, 'y':-65}, "pacchio" : {'x':0, 'y':-65}};
	
	function moveElements(pars)
	{
		var imgs = $$('img');
		
		for(var i=0; i<imgs.length; i++)
		{
			hideShownText(imgs[i]);			
		}

	}
	
	function showHiddenText(element)
	{
		new Effect.Move(element, { x: 0, y: 0, mode: 'absolute', duration: 0.75,  transition: Effect.Transitions.spring});
	}
	
	function hideShownText(element)
	{
		var movement = elementsMovement[element.id];
		
		if(movement != null)
		{
			new Effect.Move(element, { x: elementsMovement[element.id].x, y: elementsMovement[element.id].y, mode: 'absolute', duration: 0.75,  transition: Effect.Transitions.spring});
		}
		
	}
	
</script>


</head>

<body>

<div id="header-container">
	<div id="header-block">
			<span class="header-element"  onmouseover="overMenu(this);" onmouseout="outMenu(this);" onclick="goTo('home-div');" style="padding-left:0px;">
				<img src="img/menu-home-white.png" style="vertical-align:top;" />
			</span>

			<span class="header-element"  onmouseover="overMenu(this);" onmouseout="outMenu(this);" onclick="goTo('chi-div');">
				<img src="img/menu-biografi-white.png" />
				<span>
					chi siamo
				</span>
			</span>
			<span class="header-element" onmouseover="overMenu(this);" onmouseout="outMenu(this);" onclick="goTo('musica-div');">
				<img src="img/menu-discografi-white.png" />
				<span>
					musica
				</span>
			</span>
			<span class="header-element"  onmouseover="overMenu(this);" onmouseout="outMenu(this);" onclick="goTo('news-div');">
				<img src="img/menu-new-white.png" />
				<span>
					news e concerti
				</span>
			</span>
			<span class="header-element"  onmouseover="overMenu(this);" onmouseout="outMenu(this);" onclick="goTo('contatti-div');" style="padding-right:0px;">
				<img src="img/menu-contatti-white.png" />
				<span>
					contatti
				</span>
			</span>
	</div>
</div>

